using System.Collections;
using Engine.Services;
using UnityEngine;

namespace Engine.Components
{
    /// <summary>
    /// Wrapper for MonoBehaviour's coroutine methods.
    /// </summary>
    public class CoroutineProviderComponent : MonoBehaviour, ICoroutineProviderService
    {
        public new Coroutine StartCoroutine(IEnumerator routine)
        {
            Coroutine coroutine = base.StartCoroutine(routine);
            return coroutine;
        }

        public new void StopCoroutine(IEnumerator routine)
        {
            if (WasDisposed())
                return;

            base.StopCoroutine(routine);
        }

        public new void StopCoroutine(Coroutine routine)
        {
            if (WasDisposed())
                return;

            base.StopCoroutine(routine);
        }

        public new void StopAllCoroutines()
        {
            base.StopAllCoroutines();
        }

        public void Dispose()
        {
            if (WasDisposed())
                return;

            StopAllCoroutines();

            if (Application.isPlaying)
            {
                DestroyImmediate(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private bool WasDisposed()
        {
            //Check if a MonoBehaviour was Destroyed using Destroy()
            return this == null;
        }
    }
}
