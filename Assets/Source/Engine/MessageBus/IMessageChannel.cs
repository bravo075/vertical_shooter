﻿using System;

namespace Engine.MessageBus
{
    /// <summary>
    /// Message channel used to subscribe events.
    /// </summary>
    public interface IMessageChannel
    {
        int Id { get; }

        void Subscribe<T>(Action<T> callback) where T : IMessage;

        void Unsubscribe<T>(Action<T> callback);

        void UnsubscribeAll(object subscriber);

        void UnsubscribeAll();

        void ClearAllChannelSubscriptions();

        void Publish<T>(T message) where T : IMessage;
    }
}