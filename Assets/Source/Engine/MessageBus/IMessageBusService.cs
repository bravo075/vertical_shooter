﻿using System;

namespace Engine.MessageBus
{
    public interface IMessageBusService: IMessageChannel
    {
        /// <summary>
        /// Adds a new message channel to the message bus.
        /// </summary>
        /// <returns>The channel.</returns>
        /// <param name="channel">Channel.</param>
        int RegisterChannel(IMessageChannel channel);

        /// <summary>
        /// Returns the next FREE channel Id.
        /// </summary>
        /// <returns>The next unique identifier.</returns>
        int GetNextUniqueId();

        /// <summary>
        /// Subscribe the specified channel. Callback will be called every time a new 
        /// message is published on the channel.
        /// </summary>
        /// <param name="callback">Callback.</param>
        /// <param name="channel">Channel.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        void Subscribe<T>(Action<T> callback, IMessageChannel channel) where T : IMessage;

        void UnsubscribeAllFromOneChannel(object subscriber, IMessageChannel channel);

        void Publish<T>(T message, IMessageChannel channel) where T : IMessage;
    }
}