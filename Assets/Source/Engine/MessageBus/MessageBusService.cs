﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace Engine.MessageBus
{
    public class MessageBusService : IMessageBusService
    {
		//inject dependecy via interface:
        private readonly IMessageChannel _mainChannel;

        private Dictionary<int, IMessageChannel> _registeredChannels = new Dictionary<int, IMessageChannel>();

        private int _lastUsedId = -1;

        public int Id
        {
            get
            {
                return RuntimeHelpers.GetHashCode(this);
            }
        }

        public MessageBusService(IMessageChannel mainChannel)
        {
            _mainChannel = mainChannel;
            RegisterChannel(mainChannel);
        }

        public void Subscribe<T>(Action<T> callback) where T: IMessage
        {
            Subscribe(callback, _mainChannel);
        }

        public void Subscribe<T>(Action<T> callback, IMessageChannel channel) where T: IMessage
        {
            channel.Subscribe(callback);
        }

        public void Unsubscribe<T>(Action<T> callback)
        {
            Unsubscribe(callback, _mainChannel);
        }

        public void Unsubscribe<T>(Action<T> callback, IMessageChannel channel)
        {
            channel.Unsubscribe(callback);
        }

        public void UnsubscribeAllFromOneChannel(object subscriber, IMessageChannel channel)
        {
            channel.UnsubscribeAll(subscriber);
        }

        public void UnsubscribeAll()
        {
            ClearAllChannelSubscriptions();
        }

        public void UnsubscribeAll(object subscriber)
        {
            foreach (var channel in _registeredChannels.Values)
            {
                channel.UnsubscribeAll(subscriber);
            }
        }

        public void ClearAllChannelSubscriptions()
        {
            foreach (var channel in _registeredChannels.Values)
            {
                channel.ClearAllChannelSubscriptions();
            }
        }

        public void Publish<T>(T message) where T: IMessage
        {
            PublishOnAllChannels(message);
        }

        private void PublishOnAllChannels<T>(T message) where T : IMessage
        {
            foreach (var channel in _registeredChannels.Values)
            {
                channel.Publish(message);
            }
        }

        public void Publish<T>(T message, IMessageChannel channel) where T: IMessage
        {
            channel.Publish(message);
        }

        public int RegisterChannel(IMessageChannel newChannel)
        {
            var uniqueID = GetNextUniqueId();
            _registeredChannels.Add(uniqueID, newChannel);

            return uniqueID;
        }

        public IMessageChannel GetChannel(int channelId)
        {
            if (!ChannelExists(channelId))
            {
                throw new Exception("Channel " + channelId + " does not exist!");
            }

            return _registeredChannels[channelId];
        }

        private bool ChannelExists(int channelId)
        {
            return _registeredChannels.ContainsKey(channelId);
        }

        public int GetNextUniqueId()
        {
            _lastUsedId++;

			return _lastUsedId;
        }
    }
}