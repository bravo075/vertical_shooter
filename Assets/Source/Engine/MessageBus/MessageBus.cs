using System;
using System.Collections.Generic;
using Engine.Services;
using UnityEngine;

namespace Engine.MessageBus
{
    [CreateAssetMenu(fileName = "MessageBus", menuName = "Engine/Services/MessageBus")]
    public class MessageBus : BaseService, IMessageBusService
    {
        private IMessageBusService _messageBusService;

        public int Id 
        {
            get 
            { 
                return _messageBusService.Id; 
            }
        }

        private Dictionary<object, Action<Type>> _anyMessageSubscribers;
        private Dictionary<Type, Dictionary<object, Action<object>>> _anyMessageOfTypeSubscribers;

        protected override void StartService()
        {
            _anyMessageSubscribers = new Dictionary<object, Action<Type>>();
            _anyMessageOfTypeSubscribers = new Dictionary<Type, Dictionary<object, Action<object>>>();
        }

        protected override void StopService()
        {
            UnsubscribeAll();
        }

        public void SetupService(IMessageBusService messageBusService)
        {
            _messageBusService = messageBusService;
        }

        public void Subscribe<T>(Action<T> callback) where T : IMessage
        {
            _messageBusService.Subscribe(callback);
        }

        public void Subscribe<T>(Action<T> callback, IMessageChannel message) where T : IMessage
        {
            _messageBusService.Subscribe(callback, message);
        }

        public void Unsubscribe<T>(Action<T> callback)
        {
            _messageBusService.Unsubscribe(callback);
        }

        public void SubscribeToAnyMessage(Action<Type> callback)
        {
            _anyMessageSubscribers.Add(callback.Target, callback);
        }

        /// <summary>
        /// Listens for any message and its content.
        /// </summary>
        /// <param name="callback">Callback.</param>
        /// <typeparam name="T">The 1st type parameter.</typeparam>
        public void SubscribeToAnyMessageOfType<T>(Action<object> callback)
        {
            Dictionary<object, Action<object>> subscribersOfType;

            if (_anyMessageOfTypeSubscribers.TryGetValue(typeof(T), out subscribersOfType))
            {
                subscribersOfType = new Dictionary<object, Action<object>>();
                _anyMessageOfTypeSubscribers.Add(typeof(T), subscribersOfType);
            }

            subscribersOfType.Add(callback.Target, callback);
        }

        /// <summary>
        /// Unsubscribe subscribers that listen for all messages.
        /// </summary>
        /// <returns><c>true</c>, if any messages was unsubscribed, <c>false</c> otherwise.</returns>
        /// <param name="subscriber">Subscriber.</param>
        public bool UnsubscribeAnyMessages(object subscriber)
        {
            return _anyMessageSubscribers.Remove(subscriber);
        }

        public bool UnsubscribeAnyMessageOfType<T>(object subscriber)
        {
            Dictionary<object, Action<object>> subscribersOfType;
            if (!_anyMessageOfTypeSubscribers.TryGetValue(typeof(T), out subscribersOfType))
            {
                return false;
            }

            bool removed = subscribersOfType.Remove(subscriber);

            if (subscribersOfType.Count == 0)
            {
                _anyMessageOfTypeSubscribers.Remove(typeof(T));
            }

            return removed;
        }

        public void UnsubscribeAll(object subscriber)
        {
            _messageBusService?.UnsubscribeAll(subscriber);//nullable type
        }

        public void UnsubscribeAll()
        {
            _messageBusService.UnsubscribeAll();
        }

        public void ClearAllChannelSubscriptions()
        {
            _messageBusService.ClearAllChannelSubscriptions();
        }

        public void UnsubscribeAllFromOneChannel(object subscriber, IMessageChannel channel)
        {
            _messageBusService.UnsubscribeAllFromOneChannel(subscriber, channel);
        }

        public void Publish<T>(T message) where T : IMessage
        {
            _messageBusService.Publish(message);
            PublishToAnyMessageSubscribers(typeof(T));
            PublishToAnyMessageSubscribersWithContent(message);
        }

        public void Publish<T>(T message, IMessageChannel channel) where T : IMessage
        {
            _messageBusService.Publish(message, channel);
            PublishToAnyMessageSubscribers(typeof(T));
            PublishToAnyMessageSubscribersWithContent(message);
        }

        public int RegisterChannel(IMessageChannel channel)
        {
            return _messageBusService.RegisterChannel(channel);
        }

        int IMessageBusService.RegisterChannel(IMessageChannel channel)
        {
            return _messageBusService.RegisterChannel(channel);
        }

        public int GetNextUniqueId()
        {
            return _messageBusService.GetNextUniqueId();
        }

        private void PublishToAnyMessageSubscribers(Type message)
        {
            foreach (var keyValuePair in _anyMessageSubscribers)
            {
                Action<Type> callback = keyValuePair.Value;
                callback(message); 
            }
        }

        private void PublishToAnyMessageSubscribersWithContent<T>(T message) where T : IMessage
        {
            var type = typeof(T);

            foreach (var messageType in _anyMessageOfTypeSubscribers.Keys)
            {
                Dictionary<object, Action<object>> subscriberMap = null;

                if (messageType.IsAssignableFrom(type))
                {
                    subscriberMap = _anyMessageOfTypeSubscribers[messageType];
                }

                if (subscriberMap == null)
                {
                    continue;
                }

                foreach (KeyValuePair<object, Action<object>> keyValuePair in subscriberMap)
                {
                    Action<object> callbackWithContent = keyValuePair.Value;

                    //so, boxing happens here, but the message is required so what can be done.
                    callbackWithContent(message);
                }
            }
        }
    }
}
