using System;
using System.Collections;
using System.Collections.Generic;

namespace Engine.MessageBus
{
	/// <summary>
	/// The channel used for message bus subscriptions.
	/// </summary>
	public class DefaultMessageChannel : IMessageChannel
	{
		public int Id
		{
			get;
			protected set;
		}

		private readonly Dictionary<Type, Dictionary<object, IList>> _registeredActions
			= new Dictionary<Type, Dictionary<object, IList>>();

		public DefaultMessageChannel(IMessageBusService messageBusService)
		{
			if (messageBusService != null)
			{
				Id = messageBusService.RegisterChannel(this);
			}
		}

		public void Subscribe<T>(Action<T> callback) where T : IMessage
		{
			if (callback.Target == null)
			{
				throw new Exception("Delegate class instance is NULL!");
			}

			Subscribe(callback, callback.Target);
		}

		private void Subscribe<T>(Action<T> callback, object subscriber)
		{
			Type callbackType = typeof(T);

			Dictionary<object, IList> subscriptions;

			if (!_registeredActions.TryGetValue(callbackType, out subscriptions))
			{
				//object boxes the IMessage type.
				subscriptions = new Dictionary<object, IList>();
				_registeredActions.Add(callbackType, subscriptions);
			}

			IList delegates;
			if (!subscriptions.TryGetValue(subscriber, out delegates))
			{
				delegates = new List<Action<T>>();
				subscriptions.Add(subscriber, delegates);
			}

			//the actual callbacks for the subscriptions of type T:
			delegates.Add(callback);
		}

		public void UnsubscribeAll(object subscriber)
		{
			foreach (var subscriptions in _registeredActions.Values)
			{
				if (subscriptions.ContainsKey(subscriber))
				{
					subscriptions.Remove(subscriber);
				}
			}
		}

		public void UnsubscribeAll()
		{
			ClearAllChannelSubscriptions();
		}

		public void ClearAllChannelSubscriptions()
		{
			_registeredActions.Clear();
		}

		public void Unsubscribe<T>(Action<T> callback)
		{
			Type eventType = typeof(T);

			Dictionary<object, IList> subscriptions;
			IList subscriptionsPerObject;

			if (!_registeredActions.TryGetValue(eventType, out subscriptions)
				|| !subscriptions.TryGetValue(callback.Target, out subscriptionsPerObject))
			{
				return;
			}

			subscriptionsPerObject.Remove(callback);

			if (subscriptionsPerObject.Count == 0)
			{
				subscriptions.Remove(callback.Target);
			}

			if (subscriptions.Count == 0)
			{
				_registeredActions.Remove(eventType);
			}
		}

		public void Publish<T>(T message) where T : IMessage
		{
			Dictionary<object, IList> eventSubscriptions;

			if (_registeredActions.TryGetValue(typeof(T), out eventSubscriptions))
			{
				Exception caughtExceptions = null;
				IList[] subscriptionCopy = new IList[eventSubscriptions.Count];
				eventSubscriptions.Values.CopyTo(subscriptionCopy, 0);
				string lastStacktrace = string.Empty;

				foreach (IList objectSubscriptions in subscriptionCopy)
				{
					var castedList = ((List<Action<T>>)objectSubscriptions).ToArray();
					foreach (var subscription in castedList)
					{
						try
						{
							subscription(message);
						}
						catch (Exception e)
						{
							if (caughtExceptions == null)
							{
								lastStacktrace = Environment.StackTrace;
								caughtExceptions = e;
							}
						}
					}
				}

				if (caughtExceptions != null)
				{
					throw new Exception(lastStacktrace + typeof(T) + caughtExceptions);
				}
			}
		}
	}
}
