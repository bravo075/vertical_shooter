using System;

namespace Engine.Services
{
    public interface IUpdateService
    {
        void Subscribe(Action<float> action, UpdateType updateType);

        void Unsubscribe(Action<float> action, UpdateType updateType);

        void UnsubscribeAllUpdates();

        void UnsubscribeAllFixedUpdates();

        void UnsubscribeAll();

        void Update();

        void FixedUpdate();
    }
}
