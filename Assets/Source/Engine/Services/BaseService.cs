using UnityEngine;

namespace Engine.Services
{
    /// <summary>
    /// Base class for S.O. services.
    /// </summary>
    public class BaseService : ScriptableObject, IBaseService
    {
        private bool _isActive;

        public bool IsActive 
        {
            get 
            { 
                return _isActive;
            }
        }

        public void Start()
        {
            if (_isActive)
            {
                //throw new Exception("Service " + GetType().Name + " is already active");
            }

            StartService();

            _isActive = true;
        }

        public void Stop()
        {
            if (!_isActive)
            {
                //throw new Exception("Service " + GetType().Name + " is NOT active");
            }

            StopService();

            _isActive = false;
        }

        protected virtual void StartService()
        {
        }

        protected virtual void StopService()
        {
        }
    }
}
