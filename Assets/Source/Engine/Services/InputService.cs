﻿using UnityEngine;

namespace Engine.Services
{
	/// <summary>
	/// Service that handles the user inputs.
	/// </summary>
	[CreateAssetMenu(fileName = "InputService", menuName = "Engine/Services/InputService")]
	public class InputService : BaseService
	{
		public float HorizontalAxis
		{
			get
			{
				return Input.GetAxisRaw("Horizontal");
			}
		}

		public float VerticalAxis
		{
			get
			{
				return Input.GetAxisRaw("Vertical");
			}
		}

		public bool IsShootPressed
		{
			get
			{
				return Input.GetButtonDown("Fire4");
			}
		}

		public bool IsShootHeld
		{
			get
			{
				return Input.GetButton("Fire4");
			}
		}
	}
}
