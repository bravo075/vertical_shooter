﻿namespace Engine.Services
{
    /// <summary>
    /// Base interface for ScriptableObject services. 
    /// This gives me the option of injecting via Interface instead of S.O.
    /// </summary>
    public interface IBaseService
    {
        void Start();

        void Stop();

        bool IsActive
        {
            get;
        }
    }
}