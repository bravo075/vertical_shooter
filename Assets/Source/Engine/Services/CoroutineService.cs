using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Engine.Services
{
    /// <summary>
    /// Service that provides starting, stopping, or disposing coroutines.
    /// </summary>
    [CreateAssetMenu(fileName = "CoroutineService", menuName = "Engine/Services/CoroutineService")]
    public class CoroutineService : BaseService, ICoroutineProviderService
    {
        private ICoroutineProviderService _coroutineProviderService;

        public void SetupService(ICoroutineProviderService coroutineProvider)
        {
            _coroutineProviderService = coroutineProvider;
        }

        public Coroutine StartCoroutine(IEnumerator routine)
        {
            return _coroutineProviderService.StartCoroutine(routine);
        }

        public void StopCoroutine(Coroutine routine)
        {
            _coroutineProviderService.StopCoroutine(routine);
        }

        public void StopCoroutine(IEnumerator routine)
        {
            _coroutineProviderService.StopCoroutine(routine);
        }

        public void StopAllCoroutines()
        {
            _coroutineProviderService.StopAllCoroutines();
        }

        public void Dispose()
        {
            _coroutineProviderService.Dispose();
        }
    }
}
