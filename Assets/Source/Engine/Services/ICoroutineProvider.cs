using System.Collections;
using UnityEngine;

namespace Engine.Services
{
    /// <summary>
    /// Interface for the coroutine provider.
    /// </summary>
    public interface ICoroutineProviderService
    {
        Coroutine StartCoroutine(IEnumerator routine);

        void StopCoroutine(Coroutine routine);
        void StopCoroutine(IEnumerator routine);

        void StopAllCoroutines();

        void Dispose();
    }
}
