using System;
using System.Collections.Generic;
using UnityEngine;

namespace Engine.Services
{
	public enum UpdateType
	{
		Update,
		FixedUpdate
	}

	/// <summary>
	/// Service used to subscribe to a monobehavior updates.
	/// </summary>
	[CreateAssetMenu(fileName = "UpdateService", menuName = "Engine/Services/UpdateService")]
	public class UpdateService : BaseService, IUpdateService
	{
		private List<Action<float>> _updateActions;
		private List<Action<float>> _fixedUpdateActions;

		protected override void StartService()
		{
			_updateActions = new List<Action<float>>();
			_fixedUpdateActions = new List<Action<float>>();
		}

		public void Subscribe(Action<float> action, UpdateType updateType)
		{
			switch (updateType)
			{
				case UpdateType.Update:
					if (!_updateActions.Contains(action))
					{
						_updateActions.Add(action);
					}
					break;
				case UpdateType.FixedUpdate:
					if (!_fixedUpdateActions.Contains(action))
					{
						_fixedUpdateActions.Add(action);
					}
					break;
			}
		}

		public void Unsubscribe(Action<float> action, UpdateType updateType)
		{
			switch (updateType)
			{
				case UpdateType.Update:
					if (_updateActions.Contains(action))
					{
						_updateActions.Remove(action);
					}
					break;
				case UpdateType.FixedUpdate:
					if (_fixedUpdateActions.Contains(action))
					{
						_fixedUpdateActions.Remove(action);
					}
					break;
			}
		}

		public void UnsubscribeAllUpdates()
		{
			foreach (Action<float> action in _updateActions)
			{
				_updateActions.Remove(action);
			}
		}

		public void UnsubscribeAllFixedUpdates()
		{
			foreach (Action<float> action in _fixedUpdateActions)
			{
				_fixedUpdateActions.Remove(action);
			}
		}

		public void UnsubscribeAll()
		{
			UnsubscribeAllUpdates();
			UnsubscribeAllFixedUpdates();
		}

		public void Update()
		{
			float deltaTime = Time.deltaTime;
			for (int i=0; i < _updateActions.Count; i++)
			{
				TryTriggerAction(_updateActions[i], deltaTime);
			}
		}

		public void FixedUpdate()
		{
			float deltaTime = Time.deltaTime;
			for (int i = 0; i < _fixedUpdateActions.Count; i++)
			{
				TryTriggerAction(_fixedUpdateActions[i], deltaTime);
			}
		}

		private void TryTriggerAction(Action<float> action, float deltaTime)
		{
			try
			{
				action(deltaTime);
			}
			catch (Exception ex)
			{
				throw new Exception("Could not trigger action! " + ex);
			}
		}
	}
}
