using Engine.MessageBus;

namespace Game
{
    public struct OnLivesDecreasedEvent : IMessage
    {
        public int Lives;
    }
}
