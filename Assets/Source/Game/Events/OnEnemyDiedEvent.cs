using Engine.MessageBus;

namespace Game
{
    /// <summary>
    /// Triggered when an enemy is eliminated.
    /// </summary>
    public struct OnEnemyDiedEvent : IMessage
    {
        public EnemyComponent EnemyComponent;
    }
}
