using Engine.MessageBus;

namespace Game
{
    /// <summary>
    /// Processes everything related to the game over event.
    /// </summary>
    public struct OnGameOverEvent : IMessage
    {
        public int Score;
    }
}
