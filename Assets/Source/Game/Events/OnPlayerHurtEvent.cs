using Engine.MessageBus;

namespace Game
{
    public struct OnPlayerHurtEvent : IMessage
    {
        public int Health;
    }
}
