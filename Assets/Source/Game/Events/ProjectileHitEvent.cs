using Engine.MessageBus;
using UnityEngine;

namespace Game
{
	public struct ProjectileHitEvent: IMessage
	{
        public float Damage;
		public ProjectileType ProjectileType;
        public GameObject HitObject;
        public ProjectileComponent CurrentProjectile;
	}
}
