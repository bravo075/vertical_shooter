using Engine.MessageBus;

namespace Game
{
    /// <summary>
    /// When the game ends and "YOU'RE WINNER!"
    /// </summary>
    public struct OnGameWonEvent : IMessage
    {
        public int Score;
    }
}
