using Engine.MessageBus;

namespace Game
{
    public struct OnLevelEndedEvent : IMessage
    {
        public int Level;
    }
}
