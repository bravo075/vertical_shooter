using Engine.MessageBus;

namespace Game
{
    public struct OnLevelStartEvent : IMessage
    {
        public int Level;
    }
}
