using Engine.MessageBus;
using UnityEngine;

namespace Game
{
    public struct ProjectileFiredEvent: IMessage
    {
        public int EnemyHash;
        public Vector2 EnemyPosition;
        public ProjectileType ProjectileType;
    }
}
