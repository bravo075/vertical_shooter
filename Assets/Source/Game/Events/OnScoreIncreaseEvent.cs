using Engine.MessageBus;

namespace Game
{
    public struct OnScoreIncreaseEvent : IMessage
    {
        public int ScoreAmount;
    }
}
