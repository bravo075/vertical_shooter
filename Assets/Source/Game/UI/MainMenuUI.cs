using Game.Services;
using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
    public class MainMenuUI : BaseUI
    {
        [SerializeField] Button _startButton;
        [SerializeField] Button _quitButton;
        [SerializeField] Button _highScoresButton;

        [SerializeField] BaseUI _highScoresScreen;

        private ServiceContainer _serviceContainer;

        public override void Init(ServiceContainer serviceContainer)
        {
            _startButton.onClick.AddListener(OnStartPressed);
            _quitButton.onClick.AddListener(OnQuit);
            _highScoresButton.onClick.AddListener(OnHighScoresPressed);

            _serviceContainer = serviceContainer;
        }

        private void OnStartPressed()
        {
            //load level:
            _serviceContainer.MessageBus.Publish(new GameBeginEvent());
        }

        private void OnHighScoresPressed()
        {
            _highScoresScreen.Open(_serviceContainer);
        }
    }
}
