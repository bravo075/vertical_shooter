using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Game.Services;
using TMPro;

namespace Game.UI
{
    public class HighScoresUI : BaseUI
    {
        [SerializeField] Button _backButton;
        [SerializeField] TextMeshProUGUI _baseText;
        [SerializeField] Transform _textParent;

        private ServiceContainer _serviceContainer;
        private List<TextMeshProUGUI> _currentScores;

        private const string _noHighScores = "NO HIGH SCORES";
        private const string _player = "PLAYER ";
        private const string _score = " SCORE:";

        //PLAYER 1 SCORE:999999 
        public override void Open(object data = null)
        {
            base.Open(data);

            _serviceContainer = (ServiceContainer)data;

            _currentScores = new List<TextMeshProUGUI>();

            _backButton.onClick.AddListener(Close);

            ShowHighScores(_serviceContainer.SerializationService.SaveData);
        }

        public override void Close()
        {
            _backButton.onClick.RemoveAllListeners();

            base.Close();
        }

        private void ShowHighScores(SaveData saveData)
        {
            if (saveData.HighScores.Count == 0)
            {
                _baseText.gameObject.SetActive(true);
                _baseText.text = _noHighScores;
            }
            else
            {
                if (_currentScores.Count == 0)
                {
                    int counter = 0;
                    while (counter < saveData.HighScores.Count)
                    {
                        TextMeshProUGUI scoreText = Instantiate(_baseText) as TextMeshProUGUI;

                        scoreText.transform.SetParent(_textParent);
                        scoreText.transform.localScale = Vector3.one;

                        _currentScores.Add(scoreText);

                        counter++;
                    }
                }

                //load scores:
                for (int i = 0; i < _currentScores.Count; i++)
                {
                    _currentScores[i].text = _player + (i + 1) + _score + saveData.HighScores[i];
                }

                //disable base text:
                _baseText.gameObject.SetActive(false);
            }
        }
    }
}
