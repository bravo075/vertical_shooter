using Game.Services;
using UnityEngine;

namespace Game.UI
{
    public class BaseUI : MonoBehaviour
    {
        [SerializeField] GameObject GameObject;

        public virtual void Init(ServiceContainer serviceContainer)
        {
        }

        protected virtual void UpdateUi(float deltaTime)
        {
        }

        //causes boxing but it's rarely called so it's ok.
        public virtual void Open(object data = null)
        {
            GameObject.SetActive(true);
        }

        public virtual void Close()
        {
            GameObject.SetActive(false);
        }

        public virtual void OnQuit()
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
            Application.Quit();
#endif
        }
    }
}
