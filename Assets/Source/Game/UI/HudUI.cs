using Game.Services;
using UnityEngine;
using TMPro;
using System.Collections;
using UnityEngine.UI;

namespace Game.UI
{
    public class HudUI : BaseUI
    {
        [SerializeField] GameObject _notificationScreen;
        [SerializeField] GameObject _restartScreen;
        [SerializeField] TextMeshProUGUI _textCurrentLevel;
        [SerializeField] TextMeshProUGUI _textScore;
        [SerializeField] TextMeshProUGUI _textLives;
        [SerializeField] Button _buttonQuit;
        [SerializeField] Button _buttonRestart;
        [SerializeField] HudData HudData;

        private WaitForSeconds _levelScreenDelay;

        private ServiceContainer _serviceContainer;

        private const float _levelScreenTimer = 2f;

        public override void Init(ServiceContainer serviceContainer)
        {
            _serviceContainer = serviceContainer;

            ResetHudInfo(serviceContainer);

            serviceContainer.MessageBus.Subscribe<OnScoreIncreaseEvent>(UpdateScore);
            serviceContainer.MessageBus.Subscribe<OnLivesDecreasedEvent>(UpdateLives);
            serviceContainer.MessageBus.Subscribe<OnLevelStartEvent>(LevelStartScreen);
            serviceContainer.MessageBus.Subscribe<OnLevelEndedEvent>(LevelEndScreen);
            serviceContainer.MessageBus.Subscribe<OnGameOverEvent>(GameOverScreen);
            serviceContainer.MessageBus.Subscribe<OnGameWonEvent>(GameWonScreen);

            _buttonQuit.onClick.AddListener(OnQuit);
            _buttonRestart.onClick.AddListener(RestartGame);

            //cache delay:
            _levelScreenDelay = new WaitForSeconds(_levelScreenTimer);
        }

        private void ResetHudInfo(ServiceContainer serviceContainer)
        {
            _textScore.text = HudData.ScoreName + serviceContainer.GameManagerService.LevelRequirementsData.Score;
            _textLives.text = HudData.LivesName + serviceContainer.GameManagerService.LevelRequirementsData.MaxLives;
        }

        private void UpdateScore(OnScoreIncreaseEvent eventData)
        {
            _textScore.text = HudData.ScoreName + eventData.ScoreAmount;
        }

        private void UpdateLives(OnLivesDecreasedEvent eventData)
        {
            _textLives.text = HudData.LivesName + eventData.Lives;
        }

        private void LevelStartScreen(OnLevelStartEvent eventData)
        {
            _notificationScreen.SetActive(true);
            _textCurrentLevel.text = HudData.LeveName + (eventData.Level + 1);

            _serviceContainer.CoroutineService.StartCoroutine(ILevelScreenTimer());
        }

        private void LevelEndScreen(OnLevelEndedEvent eventData)
        {
            _notificationScreen.SetActive(true);
            _textCurrentLevel.text = HudData.LevelEndMessage + (eventData.Level + 1);
        }

        private void GameOverScreen(OnGameOverEvent eventData)
        {
            _notificationScreen.SetActive(true);
            _restartScreen.SetActive(true);
            _textCurrentLevel.text = HudData.GameOverMessage + "\n" + HudData.ScoreName + eventData.Score;
        }

        private void GameWonScreen(OnGameWonEvent eventData)
        {
            _notificationScreen.SetActive(true);
            _restartScreen.SetActive(true);
            _textCurrentLevel.text = HudData.WinGameMessage + "\n" + HudData.ScoreName + eventData.Score;
        }

        private void RestartGame()
        {
            _restartScreen.SetActive(false);
            ResetHudInfo(_serviceContainer);
            _serviceContainer.MessageBus.Publish(new OnRestartGameEvent());
        }

        private IEnumerator ILevelScreenTimer()
        {
            yield return _levelScreenDelay;

            //disable the level screen:
            _notificationScreen.SetActive(false);

            //This event is responsible of actually starting the game:
            _serviceContainer.MessageBus.Publish(new OnLevelScreenFinishedEvent());
        }
    }
}
