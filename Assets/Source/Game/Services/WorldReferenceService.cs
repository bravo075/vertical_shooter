using System;
using Engine.Services;
using Game.UI;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Initializes and holds a reference to the world objects.
    /// </summary>
    [CreateAssetMenu(fileName = "WorldReferenceService", menuName = "Game/Services/WorldReferenceService")]
    public class WorldReferenceService : BaseService
    {
        [NonSerialized] public Transform ViewParent;
        [NonSerialized] public Camera MainCamera;
        [NonSerialized] public PlayerComponent PlayerCharacter;
        [NonSerialized] public PlayerHealthBar PlayerHealthBar;
        [NonSerialized] public GameObject BackGround;
        [NonSerialized] public BackgroundScrollerComponent BackgroundScrollerComponent;
        [NonSerialized] public Sprite CurrentBackGroundSprite;
        [NonSerialized] public ProjectileComponent CharacterProjectileComponent;
        [NonSerialized] public ProjectileComponent EnemyProjectileComponent;
        [NonSerialized] public Transform CharacterProjectilePool;
        [NonSerialized] public Transform EnemyProjectilePool;
        [NonSerialized] public Transform EnemyPool;
        [NonSerialized] public EnemyComponent EnemyComponent;
        [NonSerialized] public MainMenuUI MainMenuUI;
        [NonSerialized] public HudUI HudUI;
        [NonSerialized] public LevelBoundsComponent LevelBoundsComponent;
        [NonSerialized] public Transform UiRoot;

        [NonSerialized] public Vector2 UpperLeftPoint;
        [NonSerialized] public Vector2 UpperRightPoint;
        [NonSerialized] public Vector2 LowerRightPoint;
        [NonSerialized] public Vector2 LowerLeftPoint;

        protected override void StartService()
        {
            GameObject viewObject = new GameObject("View");
            ViewParent = viewObject.transform;
            DontDestroyOnLoad(ViewParent);

            //projectiles:
            GameObject playerProjectilePool = new GameObject("PlayerProjectilePool");
            CharacterProjectilePool = playerProjectilePool.transform;
            CharacterProjectilePool.SetParent(ViewParent);

            GameObject enemyProjectilePool = new GameObject("EnemyProjectilePool");
            EnemyProjectilePool = enemyProjectilePool.transform;
            EnemyProjectilePool.SetParent(ViewParent);

            GameObject enemyPool = new GameObject("EnemyPool");
            EnemyPool = enemyPool.transform;
            EnemyPool.SetParent(ViewParent);
        }

        private void SetCameraBounds()
        {
            //Setup the camera bounding points:
            UpperLeftPoint = MainCamera.ViewportToWorldPoint(new Vector2(0, 1));

            UpperRightPoint = MainCamera.ViewportToWorldPoint(new Vector2(1, 1));

            LowerRightPoint = MainCamera.ViewportToWorldPoint(new Vector2(1, 0));

            LowerLeftPoint = MainCamera.ViewportToWorldPoint(new Vector2(0, 0));
        }

        public void InitCamera(GameObject inputObject)
        {
            SetupAsset(ref MainCamera, inputObject);

            if (MainCamera != null)
            {
                MainCamera.transform.SetParent(ViewParent);
                SetCameraBounds();
            }
        }

        public void InitPlayerCharacter(GameObject inputObject)
        {
            SetupAsset(ref PlayerCharacter, inputObject);

            if (PlayerCharacter != null)
            {
                PlayerCharacter.MovableEntityComponent.Transform.SetParent(ViewParent);
            }
        }

        public void InitHealthBar(GameObject inputObject)
        {
            SetupAsset(ref PlayerHealthBar, inputObject);

            if (PlayerHealthBar != null && PlayerCharacter != null)
            {
                PlayerHealthBar.transform.SetParent(PlayerCharacter.MovableEntityComponent.Transform);
            }
        }

        public void InitPlayerProjectile(GameObject inputObject)
        {
            SetupAsset(ref CharacterProjectileComponent, inputObject);

            if (CharacterProjectileComponent != null)
            {
                CharacterProjectileComponent.Transform.SetParent(CharacterProjectilePool);
                CharacterProjectileComponent.OnDespawn();
            }
        }

        public void InitEnemyProjectile(GameObject inputObject)
        {
            SetupAsset(ref EnemyProjectileComponent, inputObject);

            if (EnemyProjectileComponent != null)
            {
                EnemyProjectileComponent.Transform.SetParent(EnemyProjectilePool);
                EnemyProjectileComponent.OnDespawn();
            }
        }

        public void InitBackground(GameObject inputObject)
        {
            BackGround = inputObject;

            if (BackGround != null)
            {
                BackGround.transform.SetParent(ViewParent);

                BackgroundScrollerComponent scrollerComponent = BackGround.GetComponent<BackgroundScrollerComponent>();
                if (scrollerComponent != null)
                {
                    BackgroundScrollerComponent = scrollerComponent;
                }
            }
        }

        public void SetBackgroundSprite(UnityEngine.Object inputObject)
        {
            if (BackGround != null)
            {
                SpriteRenderer spriteRenderer = BackGround.GetComponent<SpriteRenderer>();
                Sprite sprite = GetSpriteFromTex(inputObject);

                if (spriteRenderer != null && sprite != null)
                {
                    CurrentBackGroundSprite = sprite;
                    spriteRenderer.sprite = CurrentBackGroundSprite;
                }
            }
        }

        public void SetUiRoot(GameObject inputObject)
        {
            SetupAsset(ref UiRoot, inputObject);
        }

        public void SetMainMenu(GameObject inputObject)
        {
            SetupAsset(ref MainMenuUI, inputObject);

            if (MainMenuUI != null)
            {
                MainMenuUI.transform.SetParent(UiRoot);
            }
        }

        public void SetupHud(GameObject inputObject)
        {
            SetupAsset(ref HudUI, inputObject);

            if (HudUI != null)
            {
                HudUI.transform.SetParent(UiRoot);
            }
        }

        public void InitEnemy(GameObject inputObject)
        {
            SetupAsset(ref EnemyComponent, inputObject);

            if (EnemyComponent != null)
            {
                EnemyComponent.MovableEntityComponent.Transform.SetParent(EnemyPool);
                EnemyComponent.MovableEntityComponent.GameObject.SetActive(false);
            }
        }

        public void InitLevelBounds(GameObject inputObject)
        {
            SetupAsset(ref LevelBoundsComponent, inputObject);

            if (LevelBoundsComponent != null)
            {
                LevelBoundsComponent.transform.SetParent(ViewParent);
            }
        }

        public void SetupAsset<T>(ref T inputType, GameObject objectAsset)
        {
            T objectComponent = objectAsset.GetComponent<T>();

            if (objectComponent == null)
            {
                throw new Exception("Could not get reference to: " + typeof(T));
            }
            else
            {
                inputType = objectComponent;
            }
        }

        private Sprite GetSpriteFromTex(UnityEngine.Object inputObject)
        {
            Type type = inputObject.GetType();
            Sprite sprite = inputObject as Sprite;
            Texture2D tex = inputObject as Texture2D;

            if (type == typeof(Texture2D))
            {
                sprite = Sprite.Create(inputObject as Texture2D, new Rect(0f, 0f, tex.width, tex.height),
                    new Vector2(0.5f, 0.5f));
            }

            return sprite;
        }

        public Vector2 GetEnemySpawnPosition(bool onlyTop, float offset = 0f)
        {
            Vector2 result = Vector2.zero;

            //spawn on the top part of the screen:
            if (onlyTop)
            {
                result = new Vector2(UnityEngine.Random.Range(UpperLeftPoint.x, UpperRightPoint.x),
                    UpperLeftPoint.y + offset);
            }
            else
            {
                float leftOrRight = UpperLeftPoint.x - offset;
                if (UnityEngine.Random.value < 0.5)
                    leftOrRight = UpperRightPoint.x + offset;

                result = new Vector2(leftOrRight, UnityEngine.Random.Range(UpperLeftPoint.y, LowerLeftPoint.y));
            }

            return result;
        }
    }
}
