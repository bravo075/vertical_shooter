using Engine.MessageBus;
using Engine.Services;
using UnityEngine;


namespace Game.Services
{
    /// <summary>
    /// Holds the services.
    /// </summary>
    [CreateAssetMenu(fileName = "ServiceContainer", menuName = "Engine/ServiceContainer")]
    public class ServiceContainer : ScriptableObject
    {
        public CoroutineService CoroutineService;
        public MessageBus MessageBus;
        public UpdateService UpdateService;
        public AssetLoaderService AssetLoaderService;
        public WorldReferenceService WorldReferenceService;
        public InputService InputService;
        public EntityManagerService EntityManagerService;
        public PoolService PoolService;
        public GameManagerService GameManagerService;
        public SerializationService SerializationService;
    }
}
