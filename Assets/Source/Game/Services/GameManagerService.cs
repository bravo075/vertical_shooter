using Engine.Services;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game.Services
{
    /// <summary>
    /// Handles all gameplay logic.
    /// </summary>
    [CreateAssetMenu(fileName = "GameManagerService", menuName = "Game/Services/GameManagerService")]
    public class GameManagerService : BaseService
    {
        [SerializeField] ServiceContainer _serviceContainer;
        [SerializeField] AssetBundleData _assetBundleData;
        [SerializeField] LevelRequirementsData _levelRequirementsData;

        private int _currentLevel;
        private int _currentScore;
        private int _currentLives;
        private int _currentEnemiesEliminated;
        private int _currentSpawnCounter;
        private int _enemiesRequiredPerLevel;
        private int _maxAmountOfLevelsAllowed;//used to win the game.

        private WaitForSeconds _enemySpawnDelay;

        private bool _initialSetup;
        private bool _isEnemySpawning;

        private WaitForSeconds _restartDelay;

        private Queue<IEnumerator> _enemySpawnQueue;

        public LevelRequirementsData LevelRequirementsData
        {
            get
            {
                return _levelRequirementsData;
            }
        }

        protected override void StartService()
        {
            _initialSetup = true;

            _restartDelay = new WaitForSeconds(1f);
            _maxAmountOfLevelsAllowed = _levelRequirementsData.EnemyLevelConfigs.Length - 1;

            //init collections:
            _enemySpawnQueue = new Queue<IEnumerator>();

            //add listeners:
            _serviceContainer.MessageBus.Subscribe<GameBeginEvent>(_ => OnGameBegin());
            _serviceContainer.MessageBus.Subscribe<OnEnemyDiedEvent>(OnEnemyDied);
            _serviceContainer.MessageBus.Subscribe<OnLevelScreenFinishedEvent>(_ => OnLevelScreenDone());
            _serviceContainer.MessageBus.Subscribe<OnPlayerDiedEvent>(_ => OnPlayerDied());
            _serviceContainer.MessageBus.Subscribe<OnRestartGameEvent>(_ => RestartGame());
            _serviceContainer.UpdateService.Subscribe(SpawnEnemiesUpdate, UpdateType.Update);

            //begin the main menu:
            _serviceContainer.WorldReferenceService.MainMenuUI.Init(_serviceContainer);
        }

        private void OnGameBegin()
        {
            //unload main menu:
            AssetBundles.AssetBundleManager.UnloadAssetBundle(_assetBundleData.MainMenuBundlesName, true);
            Destroy(_serviceContainer.WorldReferenceService.MainMenuUI.gameObject);

            //set initial data:
            _currentEnemiesEliminated = 0;
            _currentSpawnCounter = 0;
            _currentScore = 0;
            _currentLives = 0;
            _currentLevel = _levelRequirementsData.IntialLevel;
            _currentLives = _levelRequirementsData.MaxLives;
            _isEnemySpawning = false;

            _serviceContainer.CoroutineService.StartCoroutine(_serviceContainer.AssetLoaderService
                .ILoadInitialUi(_currentLevel, _initialSetup));
        }

        private void RestartGame()
        {
            //set initial data:
            _currentEnemiesEliminated = 0;
            _currentSpawnCounter = 0;
            _currentScore = 0;
            _currentLives = 0;
            _currentLives = _levelRequirementsData.MaxLives;
            _isEnemySpawning = false;

            RestartPlayer();

            //load level assets:
            if (_currentLevel == 0)
            {
                _serviceContainer.CoroutineService.StartCoroutine(_serviceContainer.AssetLoaderService
                    .ILoadLevelAssets(_currentLevel, _initialSetup));
            }
            else
            {
                _currentLevel = _levelRequirementsData.IntialLevel;

                //unload assets:
                _serviceContainer.AssetLoaderService.UnloadLevelData(_currentLevel);

                _serviceContainer.CoroutineService.StartCoroutine(_serviceContainer.AssetLoaderService
                    .ILoadLevelAssets(_currentLevel, _initialSetup));
            }
        }

        public void ConfigureLevel()
        {
            _enemiesRequiredPerLevel = _levelRequirementsData.EnemyLevelConfigs[_currentLevel].EnemiesRequiredPerLevel;

            //initialize enemy pool:
            _serviceContainer.PoolService.StartEnemyPool();

            //initialize HUD and bounds:
            if (_initialSetup)
            {
                _serviceContainer.WorldReferenceService.LevelBoundsComponent.SetBounds(_serviceContainer);
            }
            else
            {
                _serviceContainer.MessageBus.Publish(new OnLevelStartEvent
                {
                    Level = _currentLevel
                });
            }

            _initialSetup = false;
        }

        private void OnLevelScreenDone()
        {
            //initialize enemy queue:
            while (_enemySpawnQueue.Count < _levelRequirementsData.EnemyLevelConfigs[_currentLevel].MaxEnemiesOnScreen)
            {
                _enemySpawnQueue.Enqueue(ISpawnEnemies());
            }

            //start scrolling background:
            _serviceContainer.WorldReferenceService.BackgroundScrollerComponent.Init(_serviceContainer);
        }

        private void SpawnEnemiesUpdate(float deltaTime)
        {
            //check if enemies can be spawned:
            if (CanSpawnEnemy())
            {
                _serviceContainer.CoroutineService.StartCoroutine(_enemySpawnQueue.Peek());
                _enemySpawnQueue.Dequeue();
            }
        }

        private IEnumerator ISpawnEnemies()
        {
            _isEnemySpawning = true;

            //wait for a random time before spawning the next enemy:
            _enemySpawnDelay = new WaitForSeconds(Random.Range(_levelRequirementsData.MinTimeBetweenEnemies,
                _levelRequirementsData.MaxTimeBetweenEnemies));

            yield return _enemySpawnDelay;

            Debug.Log("<color=green>Spawned a new enemy</color>");

            //get a new enemy:
            EnemyComponent enemy = _serviceContainer.PoolService.GetEnemy();

            //set position:
            enemy.MovableEntityComponent.GlobalPosition = _serviceContainer.WorldReferenceService
                .GetEnemySpawnPosition(_levelRequirementsData.EnemyLevelConfigs[_currentLevel].SpawnOnlyOnTop,
                _levelRequirementsData.SpawnOffset);

            //initialize:
            enemy.Init(_serviceContainer);

            _currentSpawnCounter++;

            _isEnemySpawning = false;
        }

        private IEnumerator IUnloadLevel()
        {
            //should never happen but just a fail-safe check:
            if (_currentEnemiesEliminated > _levelRequirementsData.EnemyLevelConfigs[_currentLevel].EnemiesRequiredPerLevel)
                _currentEnemiesEliminated = _levelRequirementsData.EnemyLevelConfigs[_currentLevel].EnemiesRequiredPerLevel;

            _serviceContainer.AssetLoaderService.UnloadLevelData(_currentLevel);

            yield return new WaitForSeconds(2f);

            //increase level:
            _currentLevel++;

            //reset:
            ResetSpawningInfo();

            //configure new level:
            _serviceContainer.CoroutineService.StartCoroutine(_serviceContainer.AssetLoaderService
                .ILoadLevelAssets(_currentLevel, _initialSetup));
        }

        public void ResetSpawningInfo()
        {
            _currentEnemiesEliminated = 0;
            _currentSpawnCounter = 0;
            _isEnemySpawning = false;
        }

        private void ClearEnemySpawnQueue()
        {
            for (int i=0; i < _enemySpawnQueue.Count; i++)
            {
                _serviceContainer.CoroutineService.StopCoroutine(_enemySpawnQueue.Peek());
                _enemySpawnQueue.Dequeue();
            }
        }

        private void OnLevelDone()
        {
            //clear queue:
            ClearEnemySpawnQueue();
            _serviceContainer.PoolService.DestroyEnemies();

            //stop background:
            _serviceContainer.WorldReferenceService.BackgroundScrollerComponent.Stop(_serviceContainer);

            //check if this was the last level:
            if (_currentLevel == _maxAmountOfLevelsAllowed)
            {
                _serviceContainer.CoroutineService.StartCoroutine(IOnGameWon());

                return;
            }

            //new level:
            _serviceContainer.MessageBus.Publish(new OnLevelEndedEvent
            {
                Level = _currentLevel
            });

            //unload current level assets:
            _serviceContainer.CoroutineService.StartCoroutine(IUnloadLevel());
        }

        private void OnEnemyDied(OnEnemyDiedEvent eventData)
        {
            if (_currentEnemiesEliminated >= _levelRequirementsData.EnemyLevelConfigs[_currentLevel].EnemiesRequiredPerLevel)
                return;

            _currentEnemiesEliminated++;

            //increase score:
            _currentScore += eventData.EnemyComponent.EnemyBehaviourData.ScoreOnDeath;

            _serviceContainer.MessageBus.Publish(new OnScoreIncreaseEvent
            {
                ScoreAmount = _currentScore
            });

            //check if the level is done:
            if (IsLevelDone())
            {
                OnLevelDone();

                return;
            }

            //queue an enemy spawn:
            _enemySpawnQueue.Enqueue(ISpawnEnemies());
        }

        private void OnPlayerDied()
        {
            //reduce player lives:
            _currentLives--;

            if (_currentLives < 0)
                _currentLives = 0;

            _serviceContainer.MessageBus.Publish(new OnLivesDecreasedEvent
            {
                Lives = _currentLives
            });

            if (_currentLives > 0)
            {
                //reset level:
                _serviceContainer.CoroutineService.StartCoroutine(IOnLevelReset());
            }
            else
            {
                //show a game over screen and restart:
                _serviceContainer.CoroutineService.StartCoroutine(IOnGameOver());
            }
        }

        private IEnumerator IOnLevelReset()
        {
            yield return _restartDelay;

            //clear queue:
            ClearEnemySpawnQueue();
            _serviceContainer.PoolService.DeactivateEnemies();

            ResetSpawningInfo();

            //stop background:
            _serviceContainer.WorldReferenceService.BackgroundScrollerComponent.Stop(_serviceContainer);

            RestartPlayer();

            _serviceContainer.MessageBus.Publish(new OnLevelStartEvent
            {
                Level = _currentLevel
            });
        }

        private IEnumerator IOnGameOver()
        {
            yield return _restartDelay;

            //clear queue:
            ClearEnemySpawnQueue();
            _serviceContainer.PoolService.DestroyEnemies();

            //stop background:
            _serviceContainer.WorldReferenceService.BackgroundScrollerComponent.Stop(_serviceContainer);

            _serviceContainer.MessageBus.Publish(new OnGameOverEvent
            {
                Score = _currentScore
            });
        }

        private IEnumerator IOnGameWon()
        {
            yield return _restartDelay;

            ClearEnemySpawnQueue();
            _serviceContainer.PoolService.DestroyEnemies();
            _serviceContainer.WorldReferenceService.BackgroundScrollerComponent.Stop(_serviceContainer);

            _serviceContainer.MessageBus.Publish(new OnGameWonEvent
            {
                Score = _currentScore
            });

        }

        private void RestartPlayer()
        {
            //restart player:
            _serviceContainer.WorldReferenceService.PlayerCharacter.MovableEntityComponent.GameObject.SetActive(true);
            _serviceContainer.WorldReferenceService.PlayerCharacter.Init(_serviceContainer);
            _serviceContainer.WorldReferenceService.PlayerHealthBar.ResetHealthBar();
        }

        private bool IsLevelDone()
        {
            return _currentEnemiesEliminated >= _levelRequirementsData.EnemyLevelConfigs[_currentLevel].EnemiesRequiredPerLevel;
        }

        private bool CanSpawnEnemy()
        {
            //check each condition so we can get a possible early return.
            if (_serviceContainer.WorldReferenceService.PlayerCharacter == null)
                return false;

            if (_serviceContainer.WorldReferenceService.PlayerCharacter.IsDead)
                return false;

            if (IsLevelDone())
                return false;

            if (_isEnemySpawning)
                return false;

            bool isQueueActive = _enemySpawnQueue.Count > 0;

            if (!isQueueActive)
                return false;

            bool canEnemySpawnOnCurrentLevel = _currentSpawnCounter <
                        _levelRequirementsData.EnemyLevelConfigs[_currentLevel].EnemiesRequiredPerLevel;

            if (!canEnemySpawnOnCurrentLevel)
                return false;

            bool canEnemyBeOnScreen = _serviceContainer.PoolService.GetCurrentActiveEnemies() <
                        _levelRequirementsData.EnemyLevelConfigs[_currentLevel].MaxEnemiesOnScreen;

            if (!canEnemyBeOnScreen)
                return false;

            return true;
        }
    }
}
