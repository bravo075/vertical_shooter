using System.Collections;
using UnityEngine;
using Engine.Services;
using AssetBundles;
using System;

namespace Game.Services
{
    /// <summary>
	/// Used to load the assets from the asset bundles.
	/// </summary>
    [CreateAssetMenu(fileName = "AssetLoaderService", menuName = "Game/Services/AssetLoaderService")]
    public class AssetLoaderService : BaseService
	{
        [SerializeField] AssetBundleData _assetBundleData;
        [SerializeField] ServiceContainer _serviceContainer;
        [SerializeField] LevelRequirementsData _levelRequirementsData;

        protected override void StartService()
        {
            _serviceContainer.CoroutineService.StartCoroutine(Initialization());
        }

        public IEnumerator Initialization()
		{
            while (!_serviceContainer.CoroutineService.IsActive)
                yield return null;

            yield return _serviceContainer.CoroutineService.StartCoroutine(Initialize());
		}

		private void InitializeSourceURL()
		{
			AssetBundleManager.SetSourceAssetBundleURL(Application.streamingAssetsPath);

            //AssetBundleManager.SetSourceAssetBundleURL("http://www.felipe-bravo.com");
        }

        private IEnumerator Initialize()
		{
			InitializeSourceURL();

			AssetBundleLoadManifestOperation request = AssetBundleManager.Initialize();

            if (request != null)
				yield return _serviceContainer.CoroutineService.StartCoroutine(request);

            _serviceContainer.MessageBus.Publish(new AssetInitializationDoneEvent());
        }

        public IEnumerator ILoadAllInitialAssets()
        {
            //load ui root:
            yield return _serviceContainer.CoroutineService.StartCoroutine(InstantiateGameObjectAsync(_assetBundleData.UiBundlesName,
                _assetBundleData.UiRootName, _serviceContainer.WorldReferenceService.SetUiRoot));

            //load main menu:
            yield return _serviceContainer.CoroutineService.StartCoroutine(InstantiateGameObjectAsync(_assetBundleData.MainMenuBundlesName,
                _assetBundleData.MainMenuName,
                _serviceContainer.WorldReferenceService.SetMainMenu));

            //load camera:
            yield return _serviceContainer.CoroutineService.StartCoroutine(InstantiateGameObjectAsync(_assetBundleData.BaseBundlesName, _assetBundleData.CameraName,
                _serviceContainer.WorldReferenceService.InitCamera));

            _serviceContainer.MessageBus.Publish(new BaseAssetsLoadedEvent());
        }

        public IEnumerator InstantiateGameObjectAsync(string assetBundleName, string assetName, Action<GameObject> callback)
		{
            //load asset from bundle:
			AssetBundleLoadAssetOperation request = AssetBundleManager.LoadAssetAsync(assetBundleName, assetName, typeof(GameObject));

			if (request == null)
				yield break;

			yield return _serviceContainer.CoroutineService.StartCoroutine(request);

			// Get the asset.
			GameObject prefab = request.GetAsset<GameObject>();

            if (prefab != null)
            {
                GameObject instantiatedObject = Instantiate(prefab);

                if (callback != null)
                    callback(instantiatedObject);
            }
		}

        public IEnumerator InstantiateGenericAssetAsync<T>(string assetBundleName, string assetName, Action<UnityEngine.Object> callback)
        {
            AssetBundleLoadAssetOperation request = AssetBundleManager.LoadAssetAsync(assetBundleName, assetName, typeof(UnityEngine.Object));

            if (request == null)
                yield break;

            yield return _serviceContainer.CoroutineService.StartCoroutine(request);

            UnityEngine.Object prefab = request.GetAsset<UnityEngine.Object>();

            if (prefab != null)
            {
                UnityEngine.Object instantiatedObject = Instantiate(prefab);

                if (callback != null)
                    callback(instantiatedObject);
            }
        }

        public IEnumerator ILoadInitialUi(int currentLevel, bool isInitialSetup)
        {
            //load HUD:
            yield return _serviceContainer.CoroutineService.StartCoroutine(InstantiateGameObjectAsync(
                _assetBundleData.HudBundlesName, _assetBundleData.HudName,
                _serviceContainer.WorldReferenceService.SetupHud));

            _serviceContainer.WorldReferenceService.HudUI.Init(_serviceContainer);

            _serviceContainer.MessageBus.Publish(new OnLevelStartEvent
            {
                Level = currentLevel
            });

            _serviceContainer.CoroutineService.StartCoroutine(ILoadBaseAssets(currentLevel, isInitialSetup));
        }

        public IEnumerator ILoadBaseAssets(int currentLevel, bool isInitialSetup)
        {
            //load background obj:
            yield return _serviceContainer.CoroutineService.StartCoroutine(InstantiateGameObjectAsync(
                _assetBundleData.BaseBundlesName, _assetBundleData.BackgroundName,
                _serviceContainer.WorldReferenceService.InitBackground));

            //load player:
            yield return _serviceContainer.CoroutineService.StartCoroutine(InstantiateGameObjectAsync(
                _assetBundleData.BaseBundlesName, _assetBundleData.PlayerCharacterName,
                _serviceContainer.WorldReferenceService.InitPlayerCharacter));

            //load health bar:
            yield return _serviceContainer.CoroutineService.StartCoroutine(InstantiateGameObjectAsync(
                _assetBundleData.UiBundlesName, _assetBundleData.HealthBar,
                _serviceContainer.WorldReferenceService.InitHealthBar));

            //load player projectiles:
            yield return _serviceContainer.CoroutineService.StartCoroutine(InstantiateGameObjectAsync(
                _assetBundleData.BaseBundlesName, _assetBundleData.PlayerProjectileName,
                _serviceContainer.WorldReferenceService.InitPlayerProjectile));

            //load level bounds:
            yield return _serviceContainer.CoroutineService.StartCoroutine(InstantiateGameObjectAsync(
                _assetBundleData.BaseBundlesName, _assetBundleData.LevelBoundsName,
                _serviceContainer.WorldReferenceService.InitLevelBounds));

            _serviceContainer.CoroutineService.StartCoroutine(ILoadLevelAssets(currentLevel, isInitialSetup));
        }

        public IEnumerator ILoadLevelAssets(int currentLevel, bool isInitialSetup)
        {
            //load background sprite:
            yield return _serviceContainer.CoroutineService.StartCoroutine(InstantiateGenericAssetAsync<UnityEngine.Object>(
                _assetBundleData.LevelBundlesName[currentLevel],
                _assetBundleData.BackgroundLvName[currentLevel],
                _serviceContainer.WorldReferenceService.SetBackgroundSprite));

            //load enemies
            yield return _serviceContainer.CoroutineService
                .StartCoroutine(
                InstantiateGameObjectAsync(_assetBundleData.LevelBundlesName[currentLevel],
                _assetBundleData.EnemyLvName[currentLevel],
                _serviceContainer.WorldReferenceService.InitEnemy));

            //load enemy projectiles:
            yield return _serviceContainer.CoroutineService.StartCoroutine(
                InstantiateGameObjectAsync(_assetBundleData.LevelBundlesName[currentLevel],
                _assetBundleData.EnemyLvProjectileName[currentLevel],
                _serviceContainer.WorldReferenceService.InitEnemyProjectile));

            if (isInitialSetup)
                _serviceContainer.MessageBus.Publish(new LevelAssetsLoadedEvent());

            _serviceContainer.GameManagerService.ConfigureLevel();
        }

        public void UnloadLevelData(int currentLevel)
        {
            //unload level data:
            AssetBundleManager.UnloadAssetBundle(_assetBundleData.LevelBundlesName[currentLevel], true);
            Destroy(_serviceContainer.WorldReferenceService.CurrentBackGroundSprite);
            Destroy(_serviceContainer.WorldReferenceService.EnemyProjectileComponent);
            Destroy(_serviceContainer.WorldReferenceService.EnemyComponent);
        }
    }
}
