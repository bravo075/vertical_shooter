using Engine.Services;
using UnityEngine;

namespace Game.Services
{
    /// <summary>
    /// Manages the entities in the world.
    /// </summary>
    [CreateAssetMenu(fileName = "EntityManagerService", menuName = "Game/Services/EntityManagerService")]
    public class EntityManagerService : BaseService
    {
        [SerializeField] ServiceContainer _serviceContainer;
        [SerializeField] LayerData _layerData;

        protected override void StartService()
        {
            InitializeEntities();

            _serviceContainer.MessageBus.Subscribe<ProjectileFiredEvent>(OnProjectileEntityFired);
            _serviceContainer.MessageBus.Subscribe<ProjectileHitEvent>(OnProjectileHitEvent);
        }

        private void InitializeEntities()
        {
            //start player:
            _serviceContainer.WorldReferenceService.PlayerCharacter.Init(_serviceContainer);

            //start health bar:
            _serviceContainer.WorldReferenceService.PlayerHealthBar.Init(_serviceContainer);

            //start initial player projectile:
            _serviceContainer.WorldReferenceService.CharacterProjectileComponent.Init(_serviceContainer);
        }

        private void OnProjectileEntityFired(ProjectileFiredEvent eventData)
        {
            //grab a projectile from the pool:
            ProjectileComponent projectile = _serviceContainer.PoolService.GetProjectile(eventData.ProjectileType);
            projectile.Init(_serviceContainer);

            //spawn char projectile:
            switch (eventData.ProjectileType)
            {
                case ProjectileType.Enemy:
                    {
                        //set the correct position and velocity:
                        projectile.OnSpawn(eventData.EnemyPosition,
                            _serviceContainer.WorldReferenceService.PlayerCharacter.MovableEntityComponent.GlobalPosition, true);
                    }
                    break;

                case ProjectileType.Player:
                    {
                        projectile.OnSpawn(_serviceContainer.WorldReferenceService.PlayerCharacter.MovableEntityComponent.GlobalPosition,
                            Vector2.up, false);
                    }
                    break;
            }
        }

        private void OnProjectileHitEvent(ProjectileHitEvent eventData)
        {
            //when one of the bounds is hit send the projectile back to the pool:
            if (eventData.HitObject.layer == _layerData.GetDefaultLayer())
            {
                eventData.CurrentProjectile.OnDespawn();
            }
            else if (eventData.HitObject.layer == _layerData.GetPlayerLayer())
            {
                //hurt player:
                EntityBase entityBase = eventData.HitObject.GetComponent<EntityBase>();
                if (entityBase != null)
                {
                    //hurt the player on damage:
                    entityBase.OnHurt(eventData.CurrentProjectile.ProjectileData.damage);

                    _serviceContainer.MessageBus.Publish(new OnPlayerHurtEvent
                    {
                        Health = entityBase.Health
                    });

                    //check for death:
                    if (entityBase.IsDead)
                    {
                        _serviceContainer.MessageBus.Publish(new OnPlayerDiedEvent());
                    }
                }

                eventData.CurrentProjectile.OnDespawn();
            }
            else if (eventData.HitObject.layer == _layerData.GetEnemyLayer())
            {
                EnemyComponent enemyComponent = eventData.HitObject.GetComponent<EnemyComponent>();
                if (enemyComponent != null)
                {
                    //hurt enemy based on damage:
                    enemyComponent.OnHurt(eventData.CurrentProjectile.ProjectileData.damage);

                    if (enemyComponent.IsDead)
                    {
                        _serviceContainer.MessageBus.Publish(new OnEnemyDiedEvent
                        {
                            EnemyComponent = enemyComponent
                        });
                    }
                }

                eventData.CurrentProjectile.OnDespawn();
            }
        }
    }
}
