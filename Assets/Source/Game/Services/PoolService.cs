using Engine.Services;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace Game.Services
{
    /// <summary>
    /// Handles the pooling of world objects.
    /// </summary>
    [CreateAssetMenu(fileName = "PoolService", menuName = "Game/Services/PoolService")]
    public class PoolService : BaseService
    {
        [SerializeField] ServiceContainer _serviceContainer;

        protected int currentPooledCounter;

        private List<ProjectileComponent> _characterProjectiles;
        private List<ProjectileComponent> _enemyProjectiles;
        private List<EnemyComponent> _enemies;

        protected override void StartService()
        {
            if (_serviceContainer.WorldReferenceService.CharacterProjectileComponent == null)
            {
                throw new Exception("Character projectile has no reference in the World Service!");
            }
            else
            {
                _characterProjectiles = new List<ProjectileComponent>();
                _characterProjectiles.Add(_serviceContainer.WorldReferenceService.CharacterProjectileComponent);
            }
        }

        public void StartEnemyPool()
        {
            if (_serviceContainer.WorldReferenceService.EnemyComponent == null)
            {
                throw new Exception("Enemy Component has no refence in the World Service!");
            }
            else
            {
                _enemies = new List<EnemyComponent>();
                _enemies.Add(_serviceContainer.WorldReferenceService.EnemyComponent);
            }

            if (_serviceContainer.WorldReferenceService.EnemyProjectileComponent == null)
            {
                throw new Exception("Character projectile has no reference in the World Service!");
            }
            else
            {
                _enemyProjectiles = new List<ProjectileComponent>();
                _enemyProjectiles.Add(_serviceContainer.WorldReferenceService.EnemyProjectileComponent);
            }
        }

        public EnemyComponent GetEnemy()
        {
            int activeCounter = 0;

            for (int i = 0; i < _enemies.Count; i++)
            {
                if (!_enemies[i].MovableEntityComponent.IsActiveSelf)
                {
                    return _enemies[i];
                }
                else
                {
                    activeCounter++;
                }
            }

            //Get a new enemy and add it to the List.
            if (activeCounter == _enemies.Count)
            {
                EnemyComponent newEnemy = Instantiate(_serviceContainer.WorldReferenceService.EnemyComponent);
                newEnemy.MovableEntityComponent.Transform.SetParent(_serviceContainer.WorldReferenceService.EnemyPool);

                _enemies.Add(newEnemy);

                return newEnemy;
            }

            return null;
        }

        public ProjectileComponent GetProjectile(ProjectileType projectileType)
        {
            ProjectileComponent result = null;

            switch (projectileType)
            {
                case ProjectileType.Enemy:
                    {
                        result = CheckPoolForProjectile(_enemyProjectiles,
                            _serviceContainer.WorldReferenceService.EnemyProjectileComponent,
                            _serviceContainer.WorldReferenceService.EnemyProjectilePool);
                    }
                    break;
                case ProjectileType.Player:
                    {
                        result = CheckPoolForProjectile(_characterProjectiles,
                            _serviceContainer.WorldReferenceService.CharacterProjectileComponent,
                            _serviceContainer.WorldReferenceService.CharacterProjectilePool);
                    }
                    break;
            }

            return result;
        }

        public int GetCurrentActiveEnemies()
        {
            int result = 0;

            for (int i=0; i < _enemies.Count; i++)
            {
                if (_enemies[i].MovableEntityComponent.IsActiveSelf)
                    result++;
            }

            return result;
        }

        private ProjectileComponent CheckPoolForProjectile(List<ProjectileComponent> projectileList,
            ProjectileComponent projectileComponent, Transform poolTransform)
        {
            int activeCounter = 0;
            for (int i = 0; i < projectileList.Count; i++)
            {
                if (!projectileList[i].IsActiveSelf)
                {
                    return projectileList[i];
                }
                else
                {
                    activeCounter++;
                }
            }

            //all pooled projectiles are currently active, get a new one:
            if (activeCounter == projectileList.Count)
            {
                ProjectileComponent newProjectile = Instantiate(projectileComponent);
                newProjectile.Transform.SetParent(poolTransform);
                projectileList.Add(newProjectile);

                return newProjectile;
            }

            return null;
        }

        public void DeactivateEnemies()
        {
            for (int i = 0; i < _enemies.Count; i++)
            {
                _enemies[i].MovableEntityComponent.GameObject.SetActive(false);
            }
        }

        public void DestroyEnemies()
        {
            for (int i=0; i < _enemies.Count; i++)
            {
                _enemies[i].Unsubscribe();
                Destroy(_enemies[i].MovableEntityComponent.GameObject);
            }

            _enemies.Clear();

            for (int i = 0; i < _enemyProjectiles.Count; i++)
            {
                Destroy(_enemyProjectiles[i].GameObject);
            }

            _enemyProjectiles.Clear();
        }
    }
}
