using System.Collections.Generic;
using UnityEngine;
using Engine.Services;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.IO;

namespace Game.Services
{
    [Serializable]
    public struct SaveData
    {
        public List<int> HighScores;
    }

    /// <summary>
	/// Serializes and deserializes the player data to and from an external file.
	/// </summary>
    [CreateAssetMenu(fileName = "SerializationService", menuName = "Game/Services/SerializationService")]
    public class SerializationService : BaseService
    {
        [SerializeField] ServiceContainer _serviceContainer;
        private SaveData _saveData;

        public SaveData SaveData
        {
            get
            {
                return _saveData;
            }
        }

        protected override void StartService()
        {
            _saveData = new SaveData();
            _saveData.HighScores = new List<int>();

            //check if a save directory has already been created:
            if (!Directory.Exists(GetFolderLocation()))
            {
                CreateSaveDirectory(GetFolderLocation());
            }

            if (!File.Exists(GetGameFileLocation()))
            {
                Save();
            }
            else
            {
                Load();
            }

            _serviceContainer.MessageBus.Subscribe<OnGameOverEvent>(SaveOnGameOver);
            _serviceContainer.MessageBus.Subscribe<OnGameWonEvent>(SaveOnGameWon);
        }

        private void SaveOnGameOver(OnGameOverEvent eventData)
        {
            SaveOnGameEvent(eventData.Score);
        }

        private void SaveOnGameWon(OnGameWonEvent eventData)
        {
            SaveOnGameEvent(eventData.Score);
        }

        private void SaveOnGameEvent(int score)
        {
            SaveData.HighScores.Add(score);
            Save();
        }

        private void Save()
        {
            IFormatter iFormatter = new BinaryFormatter();

            //get correct file location:
            Stream file = File.Create(GetGameFileLocation());

            try
            {
                iFormatter.Serialize(file, _saveData);

                Debug.Log("<color=green>Game saved succesfully!</color>");
            }
            catch (TimeoutException tex)
            {
                Debug.LogError("!Error, could not save, time out! due to: " + tex.Source);
            }
            catch (IOException ioex)
            {
                Debug.LogError("Could not save IO exception! due to: " + ioex.Source);

            }
            catch (Exception ex)
            {
                Debug.LogError("Could not save due to: " + ex.Source);
            }
            finally
            {
                file.Close();
            }
        }

        private void Load()
        {
            IFormatter iFormatter = new BinaryFormatter();

            Stream saveFile = File.Open(GetGameFileLocation(), FileMode.Open);

            try
            {
                _saveData = (SaveData)iFormatter.Deserialize(saveFile);

                Debug.Log("<color=green>Game loaded succesfully!</color>");
            }
            catch (TimeoutException tex)
            {
                Debug.LogError("!Error, could not save, time out! due to: " + tex.Source);
            }
            catch (IOException ioex)
            {
                Debug.Log("Error loading game! IOException source: " + ioex.Source);
            }
            catch (Exception ex)
            {
                Debug.Log("Error loading game! due to: " + ex.Message);
            }
            finally
            {
                saveFile.Close();
            }
        }

        private void CreateSaveDirectory(string directoryName)
        {
            try
            {
                //create directory:
                Directory.CreateDirectory(GetFolderLocation());

                Debug.Log("<color=green>Save folder created successfully</color>");
            }
            catch (IOException ioex)
            {
                Debug.LogError("ERROR! " + ioex.Message + " " + ioex.Source);
            }
            catch (Exception ex)
            {
                Debug.LogError("ERROR! " + ex.Message + " " + ex.Source);
            }
        }

        private string GetFolderLocation()
        {
            return Application.persistentDataPath;
        }

        private string GetGameFileLocation()
        {
            return GetFolderLocation() + "/save.dat";
        }
    }
}
