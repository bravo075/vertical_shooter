namespace Game.Statemachine
{
    /// <summary>
    /// Behaviour of the enemy from lv. 1
    /// </summary>
    public class EnemyBehaviourLv1 : EnemyComponent
    {
        private MoveToPlayerState _moveToPlayerState;
        private EnemyFireState _enemyFireState;
        private RetreatState _retreatState;

        protected override void StartStateMachine()
        {
            base.StartStateMachine();

            //subscribe to state events:
            _serviceContainer.MessageBus.Subscribe<EnemyFiredStateEvent>(FireState);
            _serviceContainer.MessageBus.Subscribe<ProjectileFiredEvent>(RetreatState);
            _serviceContainer.MessageBus.Subscribe<ReachedBottomStateEvent>(RetreatState);
            _serviceContainer.MessageBus.Subscribe<ReachedRetreatPositionStateEvent>(MoveToPlayerState);

            _moveToPlayerState = new MoveToPlayerState(_serviceContainer, this);
            _enemyFireState = new EnemyFireState(_serviceContainer, this);
            _retreatState = new RetreatState(_serviceContainer, this);

            //initial state, move towards the player:
            _stateMachine.ChangeState(_moveToPlayerState);
        }

        private void MoveToPlayerState(ReachedRetreatPositionStateEvent eventData)
        {
            if (eventData.EnemyHash == GetHashCode())
                _stateMachine.ChangeState(_moveToPlayerState);
        }

        private void FireState(EnemyFiredStateEvent eventData)
        {
            if (eventData.EnemyHash == GetHashCode())
                _stateMachine.ChangeState(_enemyFireState);
        }

        private void RetreatState(ProjectileFiredEvent eventData)
        {
            //when this enemy fires:
            if (eventData.EnemyHash == GetHashCode())
                _stateMachine.ChangeState(_retreatState);
        }

        private void RetreatState(ReachedBottomStateEvent eventData)
        {
            //when this enemy has reached the bottom of the screen:
            if (eventData.EnemyHash == GetHashCode())
                _stateMachine.ChangeState(_retreatState);
        }
    }
}
