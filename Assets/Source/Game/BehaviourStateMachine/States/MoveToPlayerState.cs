using UnityEngine;
using Game.Services;

namespace Game.Statemachine
{
    /// <summary>
    /// Gets player's position after spawning and moves towards them.
    /// </summary>
    public class MoveToPlayerState : IState
    {
        private ServiceContainer _serviceContainer;

        private EnemyComponent _enemyComponent;
        private Vector3 _playerPosition;
        private Vector3 _direction;

        public MoveToPlayerState(ServiceContainer serviceContainer, EnemyComponent enemyComponent)
        {
            _serviceContainer = serviceContainer;
            _enemyComponent = enemyComponent;
        }

        public void Enter()
        {
            //get player's initial position:
            _playerPosition = _serviceContainer.WorldReferenceService.PlayerCharacter.MovableEntityComponent.GlobalPosition;

            //rotate to player:
            _direction = _playerPosition - _enemyComponent.MovableEntityComponent.GlobalPosition;
            //get the angle from the initial position to the final position:
            float angle = (Mathf.Atan2(_direction.y , _direction.x) * Mathf.Rad2Deg);
            //because of the direction of the sprite 270° need to be added to the final angle:
            _enemyComponent.MovableEntityComponent.SetGlobalRotation(Quaternion.Euler(0f, 0f, angle + 270f));
        }

        public void Execute()
        {
            //move towards the last know player's position:
            _enemyComponent.MovableEntityComponent.RigidBody.velocity = _direction * _enemyComponent.MovableEntityComponent.EntityData.Speed;

            //check if the bottom of the screen was reached:
            if (HasReachedBottom(_enemyComponent.EnemyBehaviourData.DistanceToRetreat))
            {
                _serviceContainer.MessageBus.Publish(new ReachedBottomStateEvent
                {
                    EnemyHash = _enemyComponent.GetHashCode()
                });
            }

            //check if it's close enough to the player to shoot them:
            if (GetDistanceToPlayer() < _enemyComponent.EnemyBehaviourData.DistanceToShootPlayer)
            {
                _serviceContainer.MessageBus.Publish(new EnemyFiredStateEvent
                {
                    EnemyHash = _enemyComponent.GetHashCode()
                });
            }
        }

        public void Exit()
        {
            //stop moving:
            _enemyComponent.MovableEntityComponent.RigidBody.velocity = Vector2.zero;
        }

        private float GetDistanceToPlayer()
        {
            return Vector3.Distance(_enemyComponent.MovableEntityComponent.GlobalPosition,
                _serviceContainer.WorldReferenceService.PlayerCharacter.MovableEntityComponent.GlobalPosition);
        }

        private bool HasReachedBottom(float offset)
        {
            return _enemyComponent.MovableEntityComponent.GlobalPosition.y - offset < _serviceContainer.WorldReferenceService.LowerLeftPoint.y;
        }
    }
}
