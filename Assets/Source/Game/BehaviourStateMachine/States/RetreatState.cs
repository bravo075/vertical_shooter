using UnityEngine;
using Game.Services;

namespace Game.Statemachine
{
    /// <summary>
    /// The enemy retreats after doing an action.
    /// </summary>
    public class RetreatState : IState
    {
        private EnemyComponent _enemyComponent;
        private ServiceContainer _serviceContainer;
        private Vector3 _destination;

        public RetreatState(ServiceContainer serviceContainer, EnemyComponent enemyComponent)
        {
            _enemyComponent = enemyComponent;
            _serviceContainer = serviceContainer;
        }

        public void Enter()
        {
            //get a random position to retreat:
            _destination = _serviceContainer.WorldReferenceService.GetEnemySpawnPosition(true);

            //rotate to position:
            Vector3 direction = _destination - _enemyComponent.MovableEntityComponent.GlobalPosition;
            //get the angle from the initial position to the final position:
            float angle = (Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg);
            //because of the direction of the sprite 270° need to be added to the final angle:
            _enemyComponent.MovableEntityComponent.SetGlobalRotation(Quaternion.Euler(0f, 0f, angle + 270f));
        }

        public void Execute()
        {
            //move towards the random position:
            _enemyComponent.MovableEntityComponent.RigidBody.velocity = _destination
                * _enemyComponent.MovableEntityComponent.EntityData.Speed;

            //check if destination has been reached:
            if (HasReachedDestination())
            {
                _serviceContainer.MessageBus.Publish(new ReachedRetreatPositionStateEvent
                {
                    EnemyHash = _enemyComponent.GetHashCode()
                });
            }
        }

        public void Exit()
        {
        }

        private bool HasReachedDestination()
        {
            float distanceToDestination = Vector3.Distance(_enemyComponent.MovableEntityComponent.GlobalPosition,
                _destination);

            bool movedPastTop = _enemyComponent.MovableEntityComponent.GlobalPosition.y
                - _enemyComponent.EnemyBehaviourData.DistanceToRetreat > _serviceContainer.WorldReferenceService.UpperLeftPoint.y;

            return distanceToDestination < 0.25f || movedPastTop;
        }
    }
}
