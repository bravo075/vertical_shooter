using UnityEngine;
using Game.Services;

namespace Game.Statemachine
{
    /// <summary>
    /// The enemy shoots a projectile.
    /// </summary>
    public class EnemyFireState : IState
    {
        private ServiceContainer _serviceContainer;
        private EnemyComponent _enemyComponent;
        private float _waitToShootTimer;

        public EnemyFireState(ServiceContainer serviceContainer, EnemyComponent enemyComponent)
        {
            _serviceContainer = serviceContainer;
            _enemyComponent = enemyComponent;
        }

        public void Enter()
        {
            _waitToShootTimer = _enemyComponent.EnemyBehaviourData.WaitToShootTimer;
        }

        public void Execute()
        {
            _waitToShootTimer -= Time.deltaTime;
            if (_waitToShootTimer < 0)
            {
                //send an event to fire a projectile:
                _serviceContainer.MessageBus.Publish(new ProjectileFiredEvent
                {
                    EnemyHash = _enemyComponent.GetHashCode(),
                    EnemyPosition = _enemyComponent.MovableEntityComponent.GlobalPosition,
                    ProjectileType = ProjectileType.Enemy
                });
            }
        }

        public void Exit()
        {
        }
    }
}
