namespace Game.Statemachine
{
    /// <summary>
    /// Initial spawning state.
    /// </summary>
    public class SpawnState : IState
    {
        private EnemyComponent _enemyComponent;

        public SpawnState(EnemyComponent enemyComponent)
        {
            _enemyComponent = enemyComponent;
        }

        public void Enter()
        {
            _enemyComponent.MovableEntityComponent.GameObject.SetActive(true);
        }

        public void Execute()
        {
        }

        public void Exit()
        {
        }
    }
}
