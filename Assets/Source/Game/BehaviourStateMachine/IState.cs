namespace Game.Statemachine
{
    /// <summary>
    /// Interface used to define the transitions used in the S.M.
    /// </summary>
    public interface IState
    {
        void Enter();
        void Execute();
        void Exit();
    }
}
