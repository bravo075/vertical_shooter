namespace Game.Statemachine
{
    /// <summary>
    /// Executes the state machine transitions.
    /// </summary>
    public class StateMachine
    {
        public IState currentState;

        public void ChangeState(IState nextState)
        {
            if (currentState != null)
                currentState.Exit();

            currentState = nextState;
            currentState.Enter();
        }

        public void UpdateStateMachine()
        {
            if (currentState != null)
                currentState.Execute();
        }
    }
}
