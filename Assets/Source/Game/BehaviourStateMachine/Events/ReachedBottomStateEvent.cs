using Engine.MessageBus;

namespace Game.Statemachine
{
    public struct ReachedBottomStateEvent : IMessage
    {
        public int EnemyHash;
    }
}
