using Engine.MessageBus;

namespace Game.Statemachine
{
    public struct ReachedRetreatPositionStateEvent : IMessage
    {
        public int EnemyHash;
    }
}
