using Engine.MessageBus;

namespace Game.Statemachine
{
    public struct EnemyFiredStateEvent : IMessage
    {
        public int EnemyHash;
    }
}
