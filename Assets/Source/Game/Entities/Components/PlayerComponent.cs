using UnityEngine;

namespace Game
{
	/// <summary>
	/// Class placed on the player character that will accept inputs.
	/// </summary>
	public class PlayerComponent : EntityBase
	{
		protected override void UpdateEntity(float timeDelta)
		{
			if (_serviceContainer.InputService.IsShootPressed)
			{
                _serviceContainer.MessageBus.Publish(new ProjectileFiredEvent
                {
                    ProjectileType = ProjectileType.Player
                });
			}
		}

		protected override void FixedUpdateEntity(float timeDelta)
		{
			_movementVector = new Vector2(_serviceContainer.InputService.HorizontalAxis, _serviceContainer.InputService.VerticalAxis);

			MovableEntityComponent.RigidBody.velocity = _movementVector * MovableEntityComponent.EntityData.Speed;
		}
	}
}
