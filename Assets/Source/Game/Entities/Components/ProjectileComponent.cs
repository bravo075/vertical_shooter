using UnityEngine;
using Game.Services;

namespace Game
{
	public enum ProjectileType
	{
		Enemy,
		Player
	}

    /// <summary>
    /// Component used to define a projectile.
    /// </summary>
    [RequireComponent(typeof(BoxCollider2D)), RequireComponent(typeof(Rigidbody2D))]
    public class ProjectileComponent : Entity
    {
        [SerializeField] ProjectileType _projectileType;
        [SerializeField] Rigidbody2D _rigidBody;
        [SerializeField] ProjectileData _projectileData;

        private ServiceContainer _serviceContainer;

        public ProjectileData ProjectileData
        {
            get
            {
                return _projectileData;
            }
        }

        public void Init(ServiceContainer serviceContainer)
        {
            if (_serviceContainer == null)
                _serviceContainer = serviceContainer;
        }

		private void OnCollisionEnter2D(Collision2D collision)
		{
            _serviceContainer.MessageBus.Publish(new ProjectileHitEvent
            {
                ProjectileType = _projectileType,
                Damage = _projectileData.damage,
                HitObject = collision.gameObject,
                CurrentProjectile = this
			});
		}

        public void OnSpawn(Vector2 startingPosition, Vector2 finalPosition, bool useDirection)
        {
            GlobalPosition = startingPosition;
            GameObject.SetActive(true);

            Vector2 destination = finalPosition;

            if (useDirection)
                destination = (finalPosition - startingPosition).normalized;

            //add a force in the correct direction:
            AddForce(destination * _projectileData.speed);
        }

		public void OnDespawn()
		{
			GameObject.SetActive(false);
		}

        private void AddForce(Vector2 pos)
        {
            _rigidBody.AddForce(pos, ForceMode2D.Impulse);
        }
	}
}
