﻿using UnityEngine;

namespace Game
{
	/// <summary>
	/// Contains the iformation of an entity that can move.
	/// </summary>
	[RequireComponent(typeof(BoxCollider2D)), RequireComponent(typeof(Rigidbody2D))]
	public class MovableEntityComponent : Entity
	{
		public BoxCollider2D Collider;
		public Rigidbody2D RigidBody;
		public EntityData EntityData;
 	}
}