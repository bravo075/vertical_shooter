using Game.Services;
using Game.Statemachine;
using UnityEngine;

namespace Game
{
    public enum EnemyType
    {
        LV1_Enemy,
        LV2_Enemy
    }

    /// <summary>
    /// Base enemy in charge of updating the behaviour.
    /// </summary>
    public class EnemyComponent : EntityBase
    {
        public EnemyBehaviourData EnemyBehaviourData;

        [SerializeField] protected EnemyType _enemyType;

        protected StateMachine _stateMachine;
        protected SpawnState _spawnState;

        public EnemyType EnemyType
        {
            get
            {
                return _enemyType;
            }
        }

        public override void Init(ServiceContainer serviceContainer)
        {
            base.Init(serviceContainer);

            StartStateMachine();
        }

        protected virtual void StartStateMachine()
        {
            _stateMachine = new StateMachine();
            _spawnState = new SpawnState(this);

            _stateMachine.ChangeState(_spawnState);
        }

        protected override void UpdateEntity(float timeDelta)
        {
            _stateMachine.UpdateStateMachine();
        }
    }
}
