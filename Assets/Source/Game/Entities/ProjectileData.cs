using UnityEngine;

namespace Game
{
    /// <summary>
	/// Contains the data required for a projectile
	/// </summary>
	[CreateAssetMenu(fileName = "ProjectileData", menuName = "Game/Data/ProjectileData")]
    public class ProjectileData : ScriptableObject
    {
        public float speed;
        public int damage;
    }
}
