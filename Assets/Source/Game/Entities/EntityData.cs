using UnityEngine;

namespace Game
{
	/// <summary>
	/// Contains the data required for an entity to move.
	/// </summary>
	[CreateAssetMenu(fileName = "EntityData", menuName = "Game/Data/EntityData")]
	public class EntityData: ScriptableObject
	{
		public int MaxHealth;
		public float Speed;
	}
}
