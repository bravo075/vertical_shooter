using Engine.Services;
using Game.Services;
using UnityEngine;

namespace Game
{
	/// <summary>
	/// Base class required for any movable entity.
	/// </summary>
	[RequireComponent(typeof(MovableEntityComponent))]
	public class EntityBase : MonoBehaviour
	{
		public MovableEntityComponent MovableEntityComponent;

		protected ServiceContainer _serviceContainer;

		protected Vector2 _movementVector;

        protected int _health;

        private bool _isDead;

		public bool IsDead
		{
            get
            {
                return _isDead;
            }
        }

        public int Health
        {
            get
            {
                return _health;
            }
        }

		public virtual void Init(ServiceContainer serviceContainer)
		{
            if (_serviceContainer == null)
                _serviceContainer = serviceContainer;

            _serviceContainer.UpdateService.Subscribe(UpdateEntity, UpdateType.Update);
            _serviceContainer.UpdateService.Subscribe(FixedUpdateEntity, UpdateType.FixedUpdate);

            //reset health:
            _health = MovableEntityComponent.EntityData.MaxHealth;
            _isDead = false;
        }

		protected virtual void UpdateEntity(float timeDelta)
		{
		}

		protected virtual void FixedUpdateEntity(float timeDelta)
		{
		}

        public virtual void OnHurt(int damage)
        {
            if (_isDead)
                return;

            _health -= damage;

            if (_health < 0)
                _health = 0;

            Debug.Log($"<color=green>Entity {name} was hurt by: {damage}, health is now: {_health}</color>");

            if (_health == 0)
                OnDie();
        }

		public virtual void OnDie()
        {
            Unsubscribe();

            Debug.Log($"<color=green>Entity {name} has died!</color>");

            _isDead = true;

            //do an animation or effect here.

            //deactivate object:
            MovableEntityComponent.GameObject.SetActive(false);
        }

        public virtual void Unsubscribe()
        {
            _serviceContainer.UpdateService.Unsubscribe(UpdateEntity, UpdateType.Update);
            _serviceContainer.UpdateService.Unsubscribe(FixedUpdateEntity, UpdateType.FixedUpdate);
        }
	}
}
