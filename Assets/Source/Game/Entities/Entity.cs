using UnityEngine;

namespace Game
{
	/// <summary>
	/// Returns all the components of a basic enity.
	/// </summary>
	public class Entity: MonoBehaviour
	{
		[Header("Translation and Rotation Components")]
		[SerializeField] GameObject _gameObject;
		[SerializeField] Transform _transform;

		private Vector3 _globalAngles;
        private Quaternion _globalRotation;

		public GameObject GameObject
		{
			get
			{
				return _gameObject;
			}
		}

		public Transform Transform
		{
			get
			{
				return _transform;
			}
		}

		public Vector3 GlobalPosition
		{
			get
			{
				return _transform.position;
			}
            set
            {
                _transform.position = value;
            }
		}

		public Vector3 LocalPosition
		{
			get
			{
				return _transform.localPosition;
			}
		}

		//Only provide the Z angle.
		public float GlobalAngle
		{
			get
			{
				return _transform.eulerAngles.z;
			}
		}

		public bool IsActiveSelf
		{
			get
			{
				return _gameObject.activeSelf;
			}
		}

		public void SetGlobalAngle(float input)
		{
			_globalAngles = _transform.eulerAngles;
			_globalAngles.z = input;
			_transform.eulerAngles = _globalAngles;
		}

        public void SetGlobalRotation(Quaternion rotation)
        {
            _transform.rotation = rotation;
        }

		public void SetActive(bool input)
		{
			_gameObject.SetActive(input);
		}
	}
}
