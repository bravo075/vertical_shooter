using Engine.MessageBus;
using Engine.Components;
using Game.Services;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Root class that initializes services and contains the main loop of the game.
    /// </summary>
	public class CompositionRoot : MonoBehaviour
	{
        [SerializeField] CoroutineProviderComponent _coroutineProviderComponent;
        [SerializeField] ServiceContainer _serviceContainer;

        private void Awake()
		{
            DontDestroyOnLoad(gameObject);

            _serviceContainer.MessageBus.Start();
            _serviceContainer.MessageBus.SetupService(new MessageBusService(new DefaultMessageChannel(null)));

            _serviceContainer.UpdateService.Start();

            _serviceContainer.CoroutineService.Start();
            _serviceContainer.CoroutineService.SetupService(_coroutineProviderComponent);

            _serviceContainer.WorldReferenceService.Start();

            _serviceContainer.AssetLoaderService.Start();

            _serviceContainer.SerializationService.Start();

            //subscribe to setup future managers:
            _serviceContainer.MessageBus.Subscribe<BaseAssetsLoadedEvent>(_ => InitializeGameManager());
            _serviceContainer.MessageBus.Subscribe<LevelAssetsLoadedEvent>(_ => InitializeEntityServices());
            _serviceContainer.MessageBus.Subscribe<AssetInitializationDoneEvent>(_ => LoadInitialAssets());
        }

        private void OnApplicationQuit()
		{
            _serviceContainer.MessageBus.Stop();
            _serviceContainer.UpdateService.Stop();
            _serviceContainer.CoroutineService.Stop();
            _serviceContainer.WorldReferenceService.Stop();
            _serviceContainer.AssetLoaderService.Stop();
            _serviceContainer.EntityManagerService.Stop();
            _serviceContainer.PoolService.Stop();
            _serviceContainer.GameManagerService.Stop();
            _serviceContainer.SerializationService.Stop();
        }

		private void Update()
		{
            _serviceContainer.UpdateService.Update();
		}

		private void FixedUpdate()
		{
            _serviceContainer.UpdateService.FixedUpdate();
		}

        private void LoadInitialAssets()
        {
            _serviceContainer.CoroutineService.StartCoroutine(_serviceContainer.AssetLoaderService.ILoadAllInitialAssets());
        }

        private void InitializeGameManager()
        {
            _serviceContainer.GameManagerService.Start();
        }

        private void InitializeEntityServices()
        {
            _serviceContainer.EntityManagerService.Start();
            _serviceContainer.PoolService.Start();
        }
    }
}
