using Game.Services;
using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    /// <summary>
    /// Updates the player's health bar.
    /// </summary>
    public class PlayerHealthBar : MonoBehaviour
    {
        [SerializeField] Image _healthBar;
        [SerializeField] Transform Transform;

        private float _maxHealth;

        private const float _verticalOffset = 0.22f;

        public void Init(ServiceContainer serviceContainer)
        {
            Transform.localPosition = new Vector2(0f, -_verticalOffset);

            _maxHealth = serviceContainer.WorldReferenceService.PlayerCharacter.MovableEntityComponent.EntityData.MaxHealth;

            serviceContainer.MessageBus.Subscribe<OnPlayerHurtEvent>(UpdateHealthBar);

            ResetHealthBar();
        }

        public void ResetHealthBar()
        {
            _healthBar.fillAmount = 1f;
        }

        private void UpdateHealthBar(OnPlayerHurtEvent eventData)
        {
            _healthBar.fillAmount = eventData.Health / _maxHealth;
        }
    }
}
