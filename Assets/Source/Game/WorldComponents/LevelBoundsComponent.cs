using Game.Services;
using UnityEngine;

namespace Game
{
    /// <summary>
    /// Sets the collision level bounds for the player.
    /// </summary>
    public class LevelBoundsComponent : MonoBehaviour
    {
        [SerializeField] EdgeCollider2D _upperCollider;
        [SerializeField] EdgeCollider2D _lowerCollider;
        [SerializeField] EdgeCollider2D _leftMostCollider;
        [SerializeField] EdgeCollider2D _rightMostCollider;

        [SerializeField] BoundsData _boundsData;

        public void SetBounds(ServiceContainer serviceContainer)
        {
            Vector2 upperLeftPoint = new Vector2((serviceContainer.WorldReferenceService.UpperLeftPoint.x + _boundsData.LeftMostOffset),
                (serviceContainer.WorldReferenceService.UpperLeftPoint.y + _boundsData.UpperOffet));

            Vector2 upperRightPoint = new Vector2((serviceContainer.WorldReferenceService.UpperRightPoint.x + _boundsData.RightMostOffset),
                (serviceContainer.WorldReferenceService.UpperRightPoint.y + _boundsData.UpperOffet));

            Vector2 lowerRightPoint = new Vector2((serviceContainer.WorldReferenceService.LowerRightPoint.x + _boundsData.RightMostOffset),
                (serviceContainer.WorldReferenceService.LowerRightPoint.y + _boundsData.LowerOffset));

            Vector2 lowerLeftPoint = new Vector2((serviceContainer.WorldReferenceService.LowerLeftPoint.x + _boundsData.LeftMostOffset),
                (serviceContainer.WorldReferenceService.LowerLeftPoint.y + _boundsData.LowerOffset));

            //assign points:
            _upperCollider.points = new Vector2[]
            {
                upperLeftPoint,
                upperRightPoint
            };

            _lowerCollider.points = new Vector2[]
            {
                lowerLeftPoint,
                lowerRightPoint
            };

            _leftMostCollider.points = new Vector2[]
            {
                upperLeftPoint,
                lowerLeftPoint
            };

            _rightMostCollider.points = new Vector2[]
            {
                upperRightPoint,
                lowerRightPoint
            };
        }
    }
}
