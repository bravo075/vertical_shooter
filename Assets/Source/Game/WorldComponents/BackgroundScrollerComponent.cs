using UnityEngine;
using Game.Services;
using Engine.Services;

namespace Game
{
    /// <summary>
    /// Scrolls the background using the material offset.
    /// </summary>
    public class BackgroundScrollerComponent : MonoBehaviour
    {
        private float speed = 0.05f;

        [SerializeField] SpriteRenderer _spriteRenderer;

        private Material _spriteMaterial;
        private Vector2 _currentScrollVector;

        public void Init(ServiceContainer serviceContainer)
        {
            _spriteMaterial = _spriteRenderer.material;
            _currentScrollVector = Vector2.zero;

            serviceContainer.UpdateService.Subscribe(UpdateBackgroundScroller, UpdateType.Update);
        }

        public void Stop(ServiceContainer serviceContainer)
        {
            serviceContainer.UpdateService.Unsubscribe(UpdateBackgroundScroller, UpdateType.Update);
        }

        private void UpdateBackgroundScroller(float deltaTime)
        {
            if (_spriteRenderer.sprite != null)
            {
                _currentScrollVector.y += deltaTime * speed;

                _spriteMaterial.mainTextureOffset = _currentScrollVector;

                if (_currentScrollVector.y > 1f)
                {
                    _currentScrollVector.y = 0f;
                }
            }     
        }
    }
}
