using UnityEngine;

namespace Game
{
    /// <summary>
    /// Contains the information of the layers of the world objects.
    /// </summary>
    [CreateAssetMenu(fileName = "LayerData", menuName = "Game/Data/LayerData")]
    public class LayerData : ScriptableObject
    {
        [SerializeField] string DefaultLayerName;
        [SerializeField] string PlayerLayerName;
        [SerializeField] string EnemyLayerName;

        public int GetDefaultLayer()
        {
            return LayerMask.NameToLayer(DefaultLayerName);
        }

        public int GetPlayerLayer()
        {
            return LayerMask.NameToLayer(PlayerLayerName);
        }

        public int GetEnemyLayer()
        {
            return LayerMask.NameToLayer(EnemyLayerName);
        }
    }
}
