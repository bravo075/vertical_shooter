using UnityEngine;

namespace Game
{
    /// <summary>
    /// Contains the information displayed in the HUD information.
    /// </summary>
    [CreateAssetMenu(fileName = "HudData", menuName = "Game/Data/HudData")]
    public class HudData : ScriptableObject
    {
        public string ScoreName;
        public string LivesName;
        public string LeveName;
        public string LevelEndMessage;
        public string GameOverMessage;
        public string WinGameMessage;
    }
}
