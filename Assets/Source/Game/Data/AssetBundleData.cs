using UnityEngine;

namespace Game
{
    /// <summary>
    /// Provides the asset bundles and object names.
    /// </summary>
    [CreateAssetMenu(fileName = "AssetBundleData", menuName = "Game/Data/AssetBundleData")]
    public class AssetBundleData : ScriptableObject
    {
        [Header("Bundle Names")]
        public string BaseBundlesName;
        public string UiBundlesName;
        public string MainMenuBundlesName;
        public string HudBundlesName;
        public string[] LevelBundlesName;

        [Space(5)]
        [Header("Asset Names")]
        public string CameraName;
        public string BackgroundName;
        public string PlayerCharacterName;
        public string PlayerProjectileName;
        public string LevelBoundsName;
        public string UiRootName;
        public string MainMenuName;
        public string HudName;
        public string HealthBar;
        public string[] BackgroundLvName;
        public string[] EnemyLvName;
        public string[] EnemyLvProjectileName;
    }
}
