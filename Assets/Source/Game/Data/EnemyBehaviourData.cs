using UnityEngine;

namespace Game
{
    /// <summary>
    /// Contains the information used in the enemy State Machine.
    /// </summary>
    [CreateAssetMenu(fileName = "EnemyBehaviourData", menuName = "Game/Data/EnemyBehaviourData")]
    public class EnemyBehaviourData : ScriptableObject
    {
        public float DistanceToShootPlayer;
        public float WaitToShootTimer;
        public float DistanceToRetreat;
        public int ScoreOnDeath;
    }
}
