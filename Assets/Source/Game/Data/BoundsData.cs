using UnityEngine;

namespace Game
{
    /// <summary>
    /// Provides the data for the level bounds.
    /// </summary>
    [CreateAssetMenu(fileName = "BoundsData", menuName = "Game/Data/BoundsData")]
    public class BoundsData : ScriptableObject
    {
        public float UpperOffet;
        public float LowerOffset;
        public float LeftMostOffset;
        public float RightMostOffset;
    }
}
