using UnityEngine;

namespace Game
{
    [System.Serializable]
    public struct EnemyLevelConfig
    {
        public bool SpawnOnlyOnTop;
        public int MaxEnemiesOnScreen;
        public int EnemiesRequiredPerLevel;
    }

    /// <summary>
    /// Contains the information of the layers of the world objects.
    /// </summary>
    [CreateAssetMenu(fileName = "LevelRequirementsData", menuName = "Game/Data/LevelRequirementsData")]
    public class LevelRequirementsData : ScriptableObject
    {
        public int MaxLives;
        public int IntialLevel;
        public int Score;

        public float SpawnOffset;
        public float MinTimeBetweenEnemies;
        public float MaxTimeBetweenEnemies;

        [Space(10, order = 0)]
        [Header("The size of the array determines the maximum amount of levels ", order = 1)]
        [Space(-10, order = 2)]
        [Header("required to win the game. (See also AssetBundleData)", order = 3)]
        [Space(1, order = 4)]
        public EnemyLevelConfig[] EnemyLevelConfigs;
    }
}
